var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
// var Bio = require('../models/biodata')
var bCrypt = require('bcryptjs');

module.exports = function(passport){

    passport.use('signup', new LocalStrategy({
        passReqToCallback : true
    },
    function(req, username, password, done){
        findOrCreateUser = function(){
            User.findOne({
                'username' : username
            }, function(err, user){
                if(err){
                    console.log('Error in signup : ' + err);
                    return done(err);
                }
                if(user){
                    console.log(`User ${username} already exist`);
                    return done(null, false, req.flash('message', 'User alreadyy exist'));
                } else {
                    var newUser = new User();
                    // var userBio = new Bio();

                    newUser.username = username;
                    newUser.fullname = req.body.fullname;
                    newUser.email = req.body.email;
                    newUser.password = createHash(password);


                    newUser.save(function(err){
                        if(err){
                            console.log('Error in saving user: ' + err);
                            throw err;
                        }
                        console.log('User registration succesful');
                        return done(null, newUser);
                    })

                }
            })
        }
        process.nextTick(findOrCreateUser);
    }))
    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }
}