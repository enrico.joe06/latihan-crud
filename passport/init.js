var login = require('./login');
var signup = require('./signup');
var User = require('../models/user');

module.exports = (passport) => {
    passport.serializeUser((user, done) => {
        console.log('serializing user: ', user);
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done){
        User.findById(id, (err, user) => {
            console.log('deserealizing user: ', user);
            done(err, user);
        });
    });
    login(passport);
    signup(passport);
}