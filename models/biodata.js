let mongoose = require('mongoose')

let biodataSchema = new mongoose.Schema({
    tgl_lahir: Date,
    jenis_kelamin: String,
    no_telp: String,
    alamat: String,
    foto: String,
    account: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    ]
})

module.exports = mongoose.model('Biodata', biodataSchema)
// b. Biodata
// - fullname
// - email
// - tgl lahir
// - Jenis kelamin
// - No telpon
// - alamat
// - upload foto