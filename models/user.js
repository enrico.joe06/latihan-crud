let mongoose = require('mongoose')
let validator = require('validator')
let passportLocalMongoose = require('passport-local-mongoose');

let userSchema = new mongoose.Schema({
    fullname: {
        type: String,
        require: true,
    },
    email: {
        type: String,
        require: true,
        unique: true,
        lowercase: true,
        validate: (value) => {
            return validator.isEmail(value)
        }
    },
    username: {
        type: String,
        require: true,
        unique: true
    },
    password: {
        type: String,
        require: true,
        unique: true
    },
})
userSchema.plugin(passportLocalMongoose)
module.exports = mongoose.model('User', userSchema)

// Isi database:
// a. User
// - fullname
// - email
// - username
// - password