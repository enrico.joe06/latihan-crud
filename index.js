// index.js

// Required External Modules,
const express = require("express");
const path = require("path");
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require("mongoose");
const passport = require('passport');
const expressSession = require('express-session');
const flash = require('connect-flash');
require('dotenv').config();
// const passportLocalMongoose = require('passport-local-mongoose');

// App Variables
const app = express();
const port = process.env.PORT || "8000";

// Database Connection
const url = "mongodb://localhost:27017/simple_crud";
const connectionParams={
    useNewUrlParser: true,
    useUnifiedTopology: true,
}
mongoose.connect(url, connectionParams)
    .then( () => {
        console.log('Connected to the database ')
    })
    .catch( (err) => {
        console.error(`Error connecting to the database. n${err}`);
    })

// App Configuration
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
const middlewares = [
    bodyParser.urlencoded({extended:true}),
    bodyParser.json(),
    cookieParser(),
    express.static(path.join(__dirname, "public")),
    expressSession({secret: "aabbccddeeffgg", 
                    saveUninitialized: true, 
                    resave: true}),
    passport.initialize(),
    passport.session(),
    flash()
]
app.use(middlewares);

// Passport Config
const initPassport = require('./passport/init');
initPassport(passport);

// Routes Config
const routes = require('./routes/index')(passport);
app.use('/', routes);

// Server Activation
app.listen(port, () => {
    console.log(`Listening to request on http://localhost:${port}`);
})