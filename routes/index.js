const router = require('express').Router();
const userList = require('../models/user')

var isAuthenticated = (req, res, next) => {
    if(req.isAuthenticated()) return next();

    res.redirect('/');
}

module.exports = (passport) => {
    router.get("/", (req, res) => {
        if(!req.session.passport){
        var pesan = "Silahkan registrasi terlebih dahulu";
        var masuk = false;
        res.render('home1', {pesan:pesan, masuk:masuk}); }
        else{
            res.redirect('/home')
        }
    });
    router.get("/home", isAuthenticated, (req, res) => {
        var user = req.user.fullname;
        var masuk = true;
        res.render('home', {user:user});
    })

    router.get("/profile", isAuthenticated, (req, res) => {
        res.render('user/profile');
    })
    
    router.get("/login", (req, res) => {
        if(!req.session.passport){
            res.render('Login');
        }else{
            res.redirect('/home')
        }
    });
    router.post("/login", passport.authenticate("login", {
        successRedirect: '/home',
        failureRedirect: '/',
        failureFlash: true
    }))

    router.get("/regist", (req, res) => {
        res.render("register", {title:"Pendaftaran", message:req.flash('message')});
    });
    router.post("/regist", passport.authenticate("signup", {
        successRedirect: '/home',
        failureRedirect: '/regist',
        failureFlash: true
    }));

    
    router.get("/logout", (req, res, next) => {
        req.logout((err) => {
            if (err) return next(err);
            res.redirect('/login')
        })
    })

    router.get("/data", isAuthenticated, (req, res) => {
        var data = userList.find({}, (err, data) => {
            if(err) console.log(err);
            res.render("data", {data:data});
        })
    })

    router.get("/list", (req, res) => {
        userList.find({}, (err, data) => {
            if(err) console.log(err);
            res.render("indexa", {people:data})
        })
    })

    router.post("/user/:id/delete", (req, res) => {
        userList.findByIdAndRemove({_id: req.params.id},
            (err, docs) => {
                if(err) res.json(err);
                else  res.redirect('/data');
            })
    })

    router.get("/user/:id/edit", (req, res, next) => {
        userList.findById(req.params.id, (err, doc) => {
            if (!err) {
                res.render("user/edit", {
                    title: "Edit user detail",
                    data: doc,
                    userId: req.params.id
                });
            } else {
                req.flash('error', 'User not found');
                res.redirect('/data')
            }
        })
    })
    router.post('/user/:id/update', (req, res) => {
        // req.assert('fullname', 'Nama dibutuhkan').notEmpty();
        // req.assert('email', 'Email tidak valid').isEmail();
        // var errors = req.validationErrors()

        // if(!errors) {
        userList.findByIdAndUpdate(req.params.id,{fullname:req.body.fullname,
                                                    username:req.body.username,
                                                    email:req.body.email}, (err, data) => {
                                                    if(err) {
                                                        res.send("ADA ERROOOORRR!!!!");
                                                        console.log(err);
                                                    } else res.redirect("/data");
                                                    })
        // }else{
        //     console.log(error)
        // }
    })
    return router;
}